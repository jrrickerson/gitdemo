import argparse
import tkinter as tk

from datetime import timedelta


class TimerApplication(tk.Frame):
    def __init__(self, master=None, title=""):
        super().__init__(master)
        self.master = master
        self.pack(anchor="center")
        if self.master:
            self.master.title(title)
        self.timer = timedelta()
        self.stopped = True
        self.create_widgets()
        self.update_display(self.timer)

    def create_widgets(self):
        # Entry textbox and buttons
        self.entry_frame = tk.Frame(
            self,
            padx=5,
            pady=5)
        self.entry_frame.grid(row=0, column=0)
        self.time_text = tk.Entry(
            self.entry_frame,
            font=("consolas", 24))
        self.time_text.pack(side="left")
        self.reset_button = tk.Button(
            self.entry_frame,
            font=("consolas", 24),
            text="Reset",
            command=self.on_reset)
        self.reset_button.pack(side="right")
        self.stop_button = tk.Button(
            self.entry_frame,
            font=("consolas", 24),
            text="Stop",
            command=self.on_stop)
        self.stop_button.pack(side="right")
        self.start_button = tk.Button(
            self.entry_frame,
            font=("consolas", 24),
            text="Start",
            command=self.on_start)
        self.start_button.pack(side="right")

        # Timer View
        self.timer_frame = tk.Frame(
            self,
            padx=5,
            pady=5)
        self.timer_frame.grid(row=1, column=0)
        self.time_left = tk.Label(
            self.timer_frame,
            text="00:00:00",
            font=("consolas", 48, "bold"))
        self.time_left.pack(side="top", fill="both")

    def update_timer(self, text_value):
        """Parse user input into a timedelta quantity for
        the timer to use."""
        sections = text_value.strip().split(":")
        try:
            if len(sections) == 1:
                self.timer = timedelta(
                    seconds=int(sections[0]))
            elif len(sections) == 2:
                self.timer = timedelta(
                    minutes=int(sections[0]),
                    seconds=int(sections[1]))
            elif len(sections) == 3:
                self.timer = timedelta(
                    hours=int(sections[0]),
                    minutes=int(sections[1]),
                    seconds=int(sections[2]))
            else:
                # Invalid timer input
                self.timer = timedelta()
        except ValueError:
            self.timer = timedelta()

    def update_display(self, timer):
        hours = timer.seconds // 3600
        minutes = (timer.seconds % 3600) // 60
        seconds = timer.seconds % 60

        timer_string = f"{hours:02d}:{minutes:02d}:{seconds:02d}"
        self.time_left["text"] = timer_string

    def on_start(self):
        self.stopped = False
        self.time_left.config(fg="black")
        if not self.timer:
            text_value = self.time_text.get()
            self.update_timer(text_value)
        self.on_tick()

    def on_stop(self):
        self.stopped = True

    def on_reset(self):
        self.update_timer('')
        self.update_display(self.timer)

    def on_tick(self):
        if self.stopped:
            return
        if not self.timer:
            self.stopped = True
            return
        self.timer -= timedelta(seconds=1)
        if not self.timer:
            self.time_left.config(fg="red")
        self.update_display(self.timer)
        self.time_left.after(1000, self.on_tick)

parser = argparse.ArgumentParser()
parser.add_argument('--time')
args = parser.parse_args()
root = tk.Tk()
app = TimerApplication(root, "Lab Timer")
app.mainloop()
